def oi_nome(nome):
    return f"Oie {nome}!"


def cumprimento(nome):
    """
    Essa função recebe um nome e retorna uma string com o
    seguinte formato:
    "Oie nome! Como o dia está lindo hoje."

    Dê preferência para utilizar outras funções que já estão
    prontas no corpo dessa função.
    """
    return f"{oi_nome(nome)} Como o dia está lindo hoje."
