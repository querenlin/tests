import pytest
from app.app import oi_nome, cumprimento


def test_oi_nome():
    assert oi_nome("QQ") == "Oie QQ!"
    assert oi_nome("fafa") != ""
    assert oi_nome("fafa") != None


def test_cumprimento():
    assert cumprimento("fafa") == "Oie fafa! Como o dia está lindo hoje."